from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm
from django.db.models import Count


# Create your views here.
@login_required
def receipts_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipts_list": receipts,
    }
    return render(request, "receipts/list.html", context)


@login_required
def create_receipt(request):
    # if the HTTP request is POST
    if request.method == "POST":
        # create the variable "form" as the form information from the created
        # website
        form = ReceiptForm(request.POST)
        # check to see if it is valid.  "is_valid()" is a built in method
        if form.is_valid():
            # assign form data to recipe but don't save it
            receipt = form.save(False)
            # assign current user to recipe author
            receipt.purchaser = request.user
            # save recipe(kind of like save form but form doesn't have an
            # author, recipe does)
            receipt.save()
            # use redirect to go to a recipe_list webpage
            return redirect("home")
    else:
        # if it's not a POST, create a blank RecipeForm
        form = ReceiptForm()
    # context is a naming convention to pass information to a template page
    context = {
        # creates a dict with the RecipeForm class in the variable "form"
        "form": form
    }
    # return the render with request, "website/path", context
    return render(request, "receipts/create.html", context)


@login_required
def expense_categories_list(request):
    categories = ExpenseCategory.objects.filter(owner=request.user).annotate(
        num_receipts=Count("receipts")
    )
    context = {
        "categories": categories,
    }
    return render(request, "categories/list.html", context)


@login_required
def accounts_list(request):
    accounts = Account.objects.filter(owner=request.user).annotate(
        num_receipts=Count("receipts")
    )
    context = {
        "accounts": accounts,
    }
    return render(request, "accounts/list.html", context)


@login_required
def create_expense_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()
    context = {
        "form": form,
    }
    return render(request, "categories/create.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = AccountForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/create.html", context)
